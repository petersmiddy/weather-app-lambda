import requests

def get_location(ip):
    res = requests.get(f"https://fnnb684dpf.execute-api.us-east-2.amazonaws.com/test/location?ip={ip}").json()
    return res

def get_weather(lat, lon, city):
    res = requests.get(f"https://fnnb684dpf.execute-api.us-east-2.amazonaws.com/test/forecast?city={city}&latitude={lat}&longitude={lon}").json()
    return res

def print_forecast(weather):
    summary = weather['summary']
    temp = weather['temperature']
    high = weather['high']
    low = weather['low']
    humidity = weather['humidity']
    real_feel = weather['apparentTemperature']
    print(f'The weather forecast in {city} is {summary}\n')
    print(f'{temp}\n')
    print(f'Temperature: {temp}')
    print(f'High: {high}')
    print(f'Low: {low}')
    print(f"Humidity: {humidity}")
    print(f"Feels like: {real_feel}")

if __name__ == "__main__":
    #longitude, latitude, city = get_location()
    #weather = get_temperature(longitude, latitude, city, user_defined_time)
    # print_forecast(weather)
    cords = get_location("143.115.158.25")
    lat = cords['latitude']
    lon = cords['longitude']
    city = cords['city']
    weather = get_weather(lat, lon, city)
    print_forecast(weather)