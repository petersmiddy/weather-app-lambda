from botocore.vendored import requests
import argparse
import json
from geopy.geocoders import Nominatim

def InputCityGetWeather (event, context):
  
  city = event["queryStringParameters"]["city"] 
  state = event["queryStringParameters"]["state"]

#use geopy to get latitude and longitude from city,state
  geolocator = Nominatim(user_agent="weather")
  location = geolocator.geocode(city+' '+state)
  print(location)
# print from geopy (location.latitude, location.longitude) then change to variables lat and lon
  lat = location.latitude
  lon = location.longitude
  print(lat,lon)
  coordinates = {"lat": lat, "lon": lon}
  return {
    "statusCode": 200,
    "body": json.dumps(coordinates)
  }